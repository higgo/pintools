![...](http://f.cl.ly/items/2f0X0c1E2r1D1d141R3w/y.png)

→ [View Demo](http://open.higg.im/pintools/)

The Pintools script converts any images on the page with the class-name "pinimage" to pinnable images. A pinnable image's HTML typically looks like the following:

    <img class="pinimage" src="http://example.com/assets/food1.jpg" width="342" height="262"/>

To target all images, you can simply modify line 40 of pintools.js, to make every image a pinnable item, replacing .pinimage with img.

    $('img').capty({
     cWrapper: 'capty-tile'
    });
		

Please ensure the full path to the image is contained within the src attribute of the IMG element to ensure the Pinterest service can grab the image. So for example; relative paths like the following are NOT allowed:
.

    <img src="assets/food1.jpg" width="342" height="262"/>	
		

The button calls upon the Pinterest API to browse all the images on the page. The site's visitor simply selects the image they want to pin, so long as they are logged into Pinterest. You can inlcude it anywhere on the page via the following HTML:

    <a href="#" class="browser pinBtn">Browse Images To Pin</a>
    
Pin Slider

The small slider button to the right of the page → behaves just like the Pin Browser button. The site's visitor simply selects the image they want to pin, so long as they are logged into Pinterest. All the relevant CSS for this widget are in pintools.css. So for example, if you wanted to include this to the left of the page, you would modify line 9 of that file ...
    
    left: 0;

You can include the Slider widget by adding the following HTML to your page. Preferably, just before the closing </BODY> tag.:

    <ul id="pinSlide">
    <li class="pinShare" title="Click to Pin items on this page."><img src="assets/pin.png"/>
    </li>
    </ul>

Pin Hovercard

Follow me on Pinterest
Hover over the link above to see a tooltip asking your site's visitors to follow you on Pinterest. This is an unobstrusive way of letting your site's visitors know you are on Pinterest, as the graphic is only shown upon hover. Any link that has a class-name "hovercard" will have this functionality. E.G:

    <a class="hovercard" href="http://pinterest.com/">Follow me on Pinterest</a>

Simply replace http://pinterest.com/ with the URL to your Pinterest profile.
Notes

For more on the Pinterest API, visit this page: https://developers.pinterest.com/api_docs/

